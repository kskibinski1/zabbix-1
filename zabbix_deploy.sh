#!/bin/sh
echo -e "System: \n 1)CentOS \n 2)Debian \n 3)Debian-helpers-hp&dell \n 4)Config-only"
read system

DATA=`date +%Y%m%d`

get_config(){
mv /etc/zabbix/zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf.$DATA
wget https://bitbucket.org/vibiznes/zabbix/raw/master/config/zabbix_agentd.conf -O /etc/zabbix/zabbix_agentd.conf
service zabbix-agent restart
}

get_scripts(){
rm -rf /opt/zabbix
mkdir -p /opt/zabbix
cd /opt/zabbix
wget https://bitbucket.org/vibiznes/zabbix/raw/master/scripts/scripts.list -O /opt/zabbix/scripts.list

for d in $(cat /opt/zabbix/scripts.list); do
cd /opt/zabbix
wget $d
done
rm -f /opt/zabbix/scripts.list
for i in $(ls /opt/zabbix/); do
chmod +x /opt/zabbix/$i
done
}

zabbix_sudo(){
rm -rf /tmp/zabbix-install
mkdir -p /tmp/zabbix-install
cd /tmp/zabbix-install
wget https://bitbucket.org/vibiznes/zabbix/raw/master/config/zabbix_sudoers.list
cp /etc/sudoers /etc/sudoers.$DATA
sed -i '/^Defaults:zabbix/d' /etc/sudoers
sed -i '/^zabbix/d' /etc/sudoers
cat /tmp/zabbix-install/zabbix_sudoers.list >> /etc/sudoers
}

zabbix_centos_install(){
yum install -y epel-release.noarch
yum install sudo zabbix40-agent.x86_64 -y
rm /etc/zabbix_agentd.conf
ln -s /etc/zabbix/zabbix_agentd.conf /etc/zabbix_agentd.conf
service zabbix-agent start
systemctl enable zabbix-agent.service
}

zabbix_debian_install(){
apt-get update
apt-get install sudo zabbix-agent net-tools -y
mkdir /var/log/zabbix
chown -R zabbix:zabbix /var/log/zabbix
rm -rf /tmp/zabbix-helpers
}

zabbix_debian_helpers(){
mkdir -p /tmp/zabbix-helpers
wget https://bitbucket.org/vibiznes/zabbix/downloads/hpacucli_9.20.9.0-1_amd64.deb -O /tmp/zabbix-helpers/hpacucli_9.20.9.0-1_amd64.deb
wget https://bitbucket.org/vibiznes/zabbix/downloads/megacli_8.07.14-1_amd64.deb -O /tmp/zabbix-helpers/megacli_8.07.14-1_amd64.deb
wget https://bitbucket.org/vibiznes/zabbix/downloads/megactl_0.4.1+svn20090725.r6-5_amd64.deb -O /tmp/zabbix-helpers/megactl_0.4.1+svn20090725.r6-5_amd64.deb
dpkg -i /tmp/zabbix-helpers/hpacucli_9.20.9.0-1_amd64.deb
dpkg -i /tmp/zabbix-helpers/megacli_8.07.14-1_amd64.deb
dpkg -i /tmp/zabbix-helpers/megactl_0.4.1+svn20090725.r6-5_amd64.deb
apt-get install -f
rm -rf /tmp/zabbix-helpers
rm -rf /opt/MegaRAID/MegaCli/
mkdir -p /opt/MegaRAID/MegaCli/
ln -s /usr/sbin/megacli /opt/MegaRAID/MegaCli/MegaCli64
}

if [ $system = 1 ]
then
zabbix_centos_install
fi
if [ $system = 2 ]
then
zabbix_debian_install
fi
if [ $system = 3 ]
then
zabbix_debian_helpers
fi
get_scripts
zabbix_sudo
get_config